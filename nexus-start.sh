#!/usr/bin/env bash

set -e

script_dir=`dirname "$0"`
# Wechslen in das Verzeichnis des Scriptes
cd ${script_dir}

. .env

docker-compose up -d
GW_IP=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.Gateway}}{{end}}' nexus_nexus_1)
export NEXUS=http://${GW_IP}:8081/repository
echo "Nexus-Address= $NEXUS"
